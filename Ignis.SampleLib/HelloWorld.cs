﻿using System;

namespace Synapse.SampleLib
{
    public static class HelloWorld
    {
        public static void SayHello()
        {
            Console.WriteLine("Hello, world!");
        }
    }
}